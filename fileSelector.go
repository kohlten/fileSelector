package fileSelector

import (
	"github.com/inkyblackness/imgui-go/v4"
	"github.com/veandco/go-sdl2/sdl"
	"log"
	"os"
	"runtime"
	"strings"
)

type Mode int
type Theme int

const (
	ModeOpenFile      = Mode(0)
	ModeOpenFiles     = Mode(1)
	ModeOpenDirectory = Mode(2)
	ModeOpenAny       = Mode(3)

	ThemeDark         = Theme(0)
	ThemeLight        = Theme(1)

	linuxBase   = "/"
	windowsBase = "C:/"

	maxDoubleClickTime = 300 // MS
	hoverWaitTime = 1000
	refreshTimeLimit = 500
)

var base string

var imageButtonSize = imgui.Vec2{X: 15, Y: 15}
var buttonSize = imgui.Vec2{X: 80, Y: 20}
var baseWindowSize = imgui.Vec2{X: 600, Y: 400}

type Assets struct {
	Arrow   imgui.TextureID
	Folder  imgui.TextureID
	Hidden  imgui.TextureID
	Refresh imgui.TextureID
}

type FileSelector struct {
	id                    string
	selected              string
	base                  string
	current               string
	previous              string
	lastSelected          string
	newFolderName         string
	lastSelectedTimeStamp uint64
	hoverStartTime        uint64
	lastRefresh           uint64
	mode                  Mode
	flags                 imgui.WindowFlags
	isHovering            bool
	hasSelected           bool
	showHiddenFiles       bool
	needsUpdate           bool
	open                  bool
	shiftPressed          bool
	showNewFolderDialog   bool
	calledUpdate          bool
	currentFiles          Files
	size                  imgui.Vec2
	images                Assets
	updater               *Updater
}

func init() {
	b, err := os.UserHomeDir()
	if err != nil {
		if runtime.GOOS == "windows" {
			base = windowsBase
		} else {
			base = linuxBase
		}
	} else {
		base = b
	}
}

// Loads the default images
func Init() {
	if !inited {
		loadDefaultImages()
		inited = true
	}
}

// The ID will be the name of the window.
// The baseDir is the directory to start out in. If the baseDir is blank, it will use the home directory.
// The size is how large for the selector window to be. If the size is <= 0, the size will be [600, 400]
// The mode describes how to select files. For ModeOpenDirectory, it will only show directories and you can select directories.
// If the user directory could not be found, it will start in the root directory.
// You can customize the window by providing your own assets. If you pass nil in the place of the assets, it will load the default assets
// The flags are imgui window flags.
// The theme sets what default images to use.
func NewFileSelector(id, baseDir string, mode Mode, size imgui.Vec2, images *Assets, flags imgui.WindowFlags, theme Theme) *FileSelector {
	selector := new(FileSelector)
	selector.id = id
	if baseDir != "" {
		selector.base = baseDir
	} else {
		selector.base = base
	}
	selector.current = selector.base
	selector.previous = selector.current
	selector.mode = mode
	selector.flags = flags
	if size.X > 0 && size.Y > 0 {
		selector.size = size
	} else {
		selector.size = baseWindowSize
	}
	selector.needsUpdate = true
	if images != nil {
		selector.images = *images
	} else {
		if theme == ThemeDark {
			selector.images = DefaultDarkAssets
		} else {
			selector.images = DefaultLightAssets
		}
	}
	selector.open = true
	selector.updater = NewUpdater()
	return selector
}

// Returns the current assets
func (fs *FileSelector) GetAssets() Assets {
	return fs.images
}

// If the files need to be updated, read the current directory and sort the files and directories
func (fs *FileSelector) Update() {
	if fs.needsUpdate && fs.open {
		fs.currentFiles = nil
		if !fs.updater.isUpdating && !fs.calledUpdate {
			fs.updater.Update(fs.current)
			fs.calledUpdate = true
		}
		if err := fs.updater.Error(); err != nil {
			log.Println("FS ERROR: Failed to list ", fs.current, " with error ", err.Error(), " Going to previous.")
			fs.current = fs.previous
		} else {
			if fs.updater.IsDone() {
				fs.currentFiles = fs.updater.GetFiles()
				for i := 0; i < len(fs.currentFiles); i++ {
					fs.currentFiles[i].isHidden = fs.isHidden(fs.currentFiles[i].absPath)
					fs.currentFiles[i].id = concatId(fs.currentFiles[i].name, "##", fs.id)
				}
				fs.previous = fs.current
				fs.needsUpdate = false
				fs.calledUpdate = false
				fs.selected = ""
			}
		}
	}
}

// Checks if shift if down for ModeOpenFiles, otherwise it's not needed
func (fs *FileSelector) Input(event sdl.Event) {
	switch e := event.(type) {
	case *sdl.KeyboardEvent:
		if e.Keysym.Scancode == sdl.SCANCODE_LSHIFT {
			switch e.Type {
			case sdl.KEYDOWN:
				fs.shiftPressed = true
			case sdl.KEYUP:
				fs.shiftPressed = false
			}
		}
	}
}

func (fs *FileSelector) Draw() {
	if !fs.open {
		return
	}
	isHovering := false
	imgui.SetNextWindowSizeV(fs.size, imgui.ConditionAppearing)
	imgui.BeginV(fs.id, &fs.open, fs.flags)
	size := imgui.WindowSize()
	if imgui.ImageButton(fs.images.Arrow, imageButtonSize) {
		fs.goPrevious()
	}
	if imgui.IsItemHovered() {
		isHovering = true
		if fs.hoverStartTime == 0 {
			fs.hoverStartTime = timeStamp()
		}
		if isHovering && timeStamp() - fs.hoverStartTime >= hoverWaitTime {
			imgui.BeginTooltip()
			imgui.Text("Moves to the parent directory")
			imgui.EndTooltip()
		}
	}
	imgui.SameLine()
	imgui.Text("Path: ")
	imgui.SameLine()
	padding := imgui.CurrentStyle().ItemSpacing().X
	paddingTimes := float32(12)
	offset := imgui.CalcTextSize("Path: ", false, 0).X + (imageButtonSize.X * 3) + (padding * paddingTimes) + buttonSize.X
	imgui.PushItemWidth(size.X - offset)
	if imgui.InputTextV(concatId("##", fs.id, "path"), &fs.current, imgui.InputTextFlagsEnterReturnsTrue, nil) {
		fs.needsUpdate = true
	}
	imgui.PopItemWidth()
	imgui.SameLine()
	if imgui.ImageButton(fs.images.Refresh, imageButtonSize) {
		if timeStamp() - fs.lastRefresh > refreshTimeLimit {
			fs.needsUpdate = true
			fs.lastRefresh = timeStamp()
		}
	}
	if imgui.IsItemHovered() {
		isHovering = true
		if fs.hoverStartTime == 0 {
			fs.hoverStartTime = timeStamp()
		}
		if isHovering && timeStamp() - fs.hoverStartTime >= hoverWaitTime {
			imgui.BeginTooltip()
			imgui.Text("Refreshes the files")
			imgui.EndTooltip()
		}
	}
	imgui.SameLine()
	if imgui.ImageButton(fs.images.Hidden, imageButtonSize) {
		fs.showHiddenFiles = !fs.showHiddenFiles
	}
	if imgui.IsItemHovered() {
		isHovering = true
		if fs.hoverStartTime == 0 {
			fs.hoverStartTime = timeStamp()
		}
		if isHovering && timeStamp() - fs.hoverStartTime >= hoverWaitTime {
			imgui.BeginTooltip()
			imgui.Text("Shows or removes hidden files")
			imgui.EndTooltip()
		}
	}
	imgui.SameLine()
	if imgui.ButtonV("New Folder", buttonSize) {
		fs.showNewFolderDialog = true
	}
	imgui.BeginChildV(concatId("##", "files", fs.id), imgui.Vec2{X: size.X, Y: size.Y - 130}, true, imgui.WindowFlagsAlwaysUseWindowPadding)
	for _, file := range fs.currentFiles {
		if file.isHidden && !fs.showHiddenFiles {
			continue
		}
		if !file.dir && fs.mode == ModeOpenDirectory {
			continue
		}
		if file.dir {
			imgui.Image(fs.images.Folder, imageButtonSize)
			imgui.SameLine()
		}
		if imgui.Selectable(file.id) {
			if fs.lastSelected == file.name && timeStamp()-fs.lastSelectedTimeStamp <= maxDoubleClickTime {
				fs.selectItem(file, true)
			} else {
				fs.selectItem(file, false)
			}
			fs.lastSelected = file.name
			fs.lastSelectedTimeStamp = timeStamp()
		}
	}
	imgui.EndChild()
	imgui.Dummy(imgui.Vec2{Y: 5})
	imgui.Dummy(imgui.Vec2{X: 10})
	imgui.SameLine()
	imgui.Text("File: ")
	imgui.SameLine()
	imgui.PushItemWidth(size.X - imgui.CalcTextSize("File: ", false, 0).X*2 - 10)
	imgui.InputText(concatId("##Selected", fs.id), &fs.selected)
	imgui.PopItemWidth()
	imgui.Dummy(imgui.Vec2{Y: 5})
	imgui.Dummy(imgui.Vec2{X: size.X/4 - buttonSize.X})
	imgui.SameLine()
	if imgui.ButtonV(concatId("Cancel##", fs.id), buttonSize) {
		fs.open = false
	}
	imgui.SameLine()
	imgui.Dummy(imgui.Vec2{X: ((size.X / 4) * 2) - buttonSize.X})
	imgui.SameLine()
	if imgui.ButtonV(concatId("Open##", fs.id), buttonSize) {
		if fs.selected != "" {
			fs.hasSelected = true
			fs.open = false
		}
	}
	imgui.End()
	if fs.showNewFolderDialog {
		newFolderSize := imgui.Vec2{X: 300, Y: 100}
		imgui.SetNextWindowSizeV(newFolderSize, imgui.ConditionAppearing)
		imgui.BeginV(concatId("##", fs.id, "newFolderDialog"), &fs.showNewFolderDialog, fs.flags|imgui.WindowFlagsNoResize)
		imgui.Dummy(imgui.Vec2{Y: 10})
		imgui.Dummy(imgui.Vec2{X: 10})
		imgui.SameLine()
		imgui.Text("Name: ")
		imgui.SameLine()
		imgui.PushItemWidth(newFolderSize.X - imgui.CalcTextSize("Name: ", false, 0).X*2 - 10)
		imgui.InputText(concatId("##", fs.id, "FolderName"), &fs.newFolderName)
		imgui.PopItemWidth()
		imgui.Dummy(imgui.Vec2{Y: 10})
		imgui.Dummy(imgui.Vec2{X: newFolderSize.X/4 - buttonSize.X/2})
		imgui.SameLine()
		if imgui.ButtonV("Cancel", buttonSize) {
			fs.showNewFolderDialog = false
		}
		imgui.SameLine()
		imgui.Dummy(imgui.Vec2{X: (newFolderSize.X / 4) - buttonSize.X/2})
		imgui.SameLine()
		if imgui.ButtonV("Ok", buttonSize) && fs.newFolderName != "" {
			fs.showNewFolderDialog = false
			err := os.Mkdir(getAbsPath(fs.current, fs.newFolderName), os.ModeDir|os.ModePerm)
			if err != nil {
				log.Println("Failed to create dir with error ", err.Error())
			}
			fs.newFolderName = ""
			fs.needsUpdate = true
		}
		imgui.End()
	}
	if !isHovering {
		fs.hoverStartTime = 0
	}
}

// Returns the current selected item
func (fs *FileSelector) GetSelected() string {
	return fs.selected
}

// Returns if the selector has selected an item
func (fs *FileSelector) HasSelected() bool {
	return fs.hasSelected
}

// Returns if this window is currently open
func (fs *FileSelector) IsOpen() bool {
	return fs.open
}

// Sets if the window is open
func (fs *FileSelector) SetOpen(open bool) {
	fs.open = open
}

// Finishes everything up
func (fs *FileSelector) Finish() {
	fs.updater.Finish()
}

// Resets the internal states
func (fs *FileSelector) Reset() {
	fs.selected = ""
	fs.hasSelected = false
	fs.current = fs.base
	fs.currentFiles = nil
	fs.needsUpdate = true
	fs.lastSelected = ""
	fs.showNewFolderDialog = false
	fs.newFolderName = ""
	fs.showHiddenFiles = false
}

func (fs *FileSelector) selectItem(item File, double bool) {
	if double && item.dir {
		fs.current = item.absPath
		fs.needsUpdate = true
		fs.selected = ""
	}
	if fs.mode == ModeOpenAny {
		fs.selected = item.absPath
	} else if fs.mode == ModeOpenFile && !item.dir {
		fs.selected = item.absPath
	} else if fs.mode == ModeOpenFiles && !item.dir {
		if fs.shiftPressed {
			fs.appendFile(item.absPath)
		} else {
			fs.selected = item.absPath
		}
	} else if fs.mode == ModeOpenDirectory && item.dir {
		fs.selected = item.absPath
	}
}

func (fs *FileSelector) goPrevious() {
	// -2 if the current has a trailing / "/home/blah/"
	for i := len(fs.current) - 2; i >= 0; i-- {
		if fs.current[i] == '/' {
			fs.current = fs.current[:i+1]
			fs.needsUpdate = true
			return
		}
	}
}

func (fs *FileSelector) appendFile(name string) {
	if fs.selected == "" {
		fs.selected = name
	} else {
		fs.selected += "," + name
	}
}

func concatId(args ...string) string {
	builder := strings.Builder{}
	for _, v := range args {
		builder.WriteString(v)
	}
	return builder.String()
}
