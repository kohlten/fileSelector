// +build !windows

package fileSelector

import "path/filepath"

func (fs *FileSelector) isHidden(path string) bool {
	if len(path) > 0 {
		return filepath.Base(path)[0] == '.'
	}
	return false
}
