package fileSelector

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"sort"
	"strings"
)

type Updater struct {
	isDone     bool
	isUpdating bool
	running    bool
	err        error
	data       Files
	currentDir string
	start      chan bool
}

func NewUpdater() *Updater {
	u := new(Updater)
	u.running = true
	u.start = make(chan bool)
	u.isDone = true
	go u.update()
	return u
}

func (u *Updater) Update(currentDir string) {
	if u.isUpdating {
		fmt.Println("Tried to update while updating")
		return
	}
	u.isDone = false
	u.isUpdating = true
	u.currentDir = currentDir
	u.start <- true
}

func (u *Updater) update() {
	for u.running {
	start:
		<-u.start
		if _, err := os.Stat(u.currentDir); os.IsNotExist(err) {
			u.err = err
			goto start
		}
		items, err := ioutil.ReadDir(u.currentDir)
		if err != nil {
			u.err = err
			goto start
		}
		data := u.createFiles(u.currentDir, items)
		sort.Sort(data)
		u.data = data
		u.isDone = true
		u.isUpdating = false
	}
}

func (u *Updater) GetFiles() Files {
	return u.data
}

func (u *Updater) IsDone() bool {
	return u.isDone
}

func (u *Updater) IsUpdating() bool {
	return u.isUpdating
}

func (u *Updater) Error() error {
	return u.err
}

func (u *Updater) Finish() {
	u.running = false
	u.start <- true
}

func (u *Updater) createFiles(base string, files []os.FileInfo) Files {
	ret := make([]File, len(files))
	for i, f := range files {
		isDir := f.IsDir()
		name := f.Name()
		abs := getAbsPath(base, name)
		ret[i] = File{name: name, dir: isDir, absPath: abs}
	}
	return ret
}

func getAbsPath(path, name string) string {
	if filepath.Base(path) == name {
		return path
	}
	builder := strings.Builder{}
	builder.WriteString(path)
	if path[len(path)-1] != '/' {
		builder.WriteString("/")
	}
	builder.WriteString(name)
	return builder.String()
}
