package fileSelector

import (
	"github.com/go-gl/gl/v3.2-core/gl"
	"github.com/inkyblackness/imgui-go/v4"
	"image"
	"image/draw"
	_ "image/jpeg"
	_ "image/png"
	"log"
	"os"
	"path/filepath"
	"runtime"
)

var pkgPath string
var DefaultLightAssets Assets
var DefaultDarkAssets Assets
var inited = false

func init() {
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		log.Fatalln("FS ERROR: Failed to get package path")
	}
	pkgPath = filepath.Dir(filename)
}

func loadDefaultImages() {
	DefaultDarkAssets = Assets{
		Arrow:   loadImage(pkgPath + "/assets/arrow_dark.png"),
		Folder:  loadImage(pkgPath + "/assets/folder_dark.png"),
		Hidden:  loadImage(pkgPath + "/assets/hidden_dark.png"),
		Refresh: loadImage(pkgPath + "/assets/refresh_dark.png"),
	}
	DefaultLightAssets = Assets{
		Arrow:   loadImage(pkgPath + "/assets/arrow_light.png"),
		Folder:  loadImage(pkgPath + "/assets/folder_light.png"),
		Hidden:  loadImage(pkgPath + "/assets/hidden_light.png"),
		Refresh: loadImage(pkgPath + "/assets/refresh_light.png"),
	}
}

func loadImage(filename string) imgui.TextureID {
	imgData, err := os.Open(filename)
	if err != nil {
		log.Fatalln("FS ERROR: Failed to open asset ", filename, " with error ", err)
	}
	defer imgData.Close()
	img, _, err := image.Decode(imgData)
	if err != nil {
		log.Fatalln("FS ERROR: Failed to decode asset ", filename, " with error ", err)
	}
	rgba := image.NewRGBA(img.Bounds())
	size := img.Bounds().Size()
	draw.Draw(rgba, rgba.Bounds(), img, image.Pt(0, 0), draw.Src)
	var id uint32
	gl.GenTextures(1, &id)
	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, id)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
	gl.TexImage2D(gl.TEXTURE_2D, 0, gl.RGBA, int32(size.X), int32(size.Y), 0, gl.RGBA, gl.UNSIGNED_BYTE, gl.Ptr(rgba.Pix))
	return imgui.TextureID(id)
}
