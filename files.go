package fileSelector

import (
	"strconv"
)

type File struct {
	name string
	id string
	absPath string
	isHidden bool
	dir bool
}

type Files []File

func (f Files) Len() int {
	return len(f)
}

func (f Files) Swap(i, j int) {
	f[i], f[j] = f[j], f[i]
}

func (f Files) Less(i, j int) bool {
	if f[i].dir && !f[j].dir {
		return true
	} else if !f[i].dir && f[j].dir {
		return false
	}
	if isNumeric(f[i].name) && isNumeric(f[j].name) {
		return compareNumeric(f[i], f[j])
	}
	return f[i].name < f[j].name
}

func isNumeric(name string) bool {
	for _, c := range name {
		if c == '.' {
			break
		}
		if c > '9' || c < '0' {
			return false
		}

	}
	return true
}

func getName(name string) string {
	for i, c := range name {
		if c == '.' {
			return name[:i + 1]
		}
	}
	return name
}

func compareNumeric(a, b File) bool {
	aName, _ := strconv.ParseInt(getName(a.name), 10, 64)
	bName, _ := strconv.ParseInt(getName(b.name), 10, 64)
	return aName < bName
}