package fileSelector

import "time"

func timeStamp() uint64 {
	return uint64(time.Now().UnixNano()) / uint64(time.Millisecond)
}
