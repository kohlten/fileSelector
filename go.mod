module gitlab.com/kohlten/fileSelector

go 1.14

require (
	github.com/go-gl/gl v0.0.0-20190320180904-bf2b1f2f34d7
	github.com/inkyblackness/imgui-go/v4 v4.0.1
	github.com/veandco/go-sdl2 v0.4.5
)
