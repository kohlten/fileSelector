package main

import (
	"github.com/inkyblackness/imgui-go/v4"
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/kohlten/fileSelector"
	"gitlab.com/kohlten/fileSelector/test/imguiImpl"
	"gitlab.com/kohlten/gamelib"
	"gitlab.com/kohlten/glLib"
	"log"
	"os"
	"runtime"
	"runtime/pprof"
	"strconv"
)

type UI struct {
	Window   *glLib.Window
	Platform *imguiImpl.SDL
	Renderer *imguiImpl.OpenGL3
	Ctx      *imgui.Context
	Io       imgui.IO
	selector *fileSelector.FileSelector
	fps *gamelib.FpsCounter
	running  bool
}

func NewUI() *UI {
	ui := new(UI)
	ui.initWindow()
	ui.initSelector()
	ui.running = true
	ui.fps = gamelib.NewFpsCounter()
	imgui.StyleColorsDark()
	return ui
}

func (ui *UI) initSelector() {
	fileSelector.Init()
	ui.selector = fileSelector.NewFileSelector("selector", "/home/kohlten/fileTestDir", fileSelector.ModeOpenAny, imgui.Vec2{X: 600, Y: 400}, nil, 0, fileSelector.ThemeLight)
	assets := fileSelector.DefaultLightAssets
	ui.Renderer.AddImage(uint32(assets.Folder))
	ui.Renderer.AddImage(uint32(assets.Arrow))
	ui.Renderer.AddImage(uint32(assets.Refresh))
	ui.Renderer.AddImage(uint32(assets.Hidden))
	assets = fileSelector.DefaultDarkAssets
	ui.Renderer.AddImage(uint32(assets.Folder))
	ui.Renderer.AddImage(uint32(assets.Arrow))
	ui.Renderer.AddImage(uint32(assets.Refresh))
	ui.Renderer.AddImage(uint32(assets.Hidden))
}

func (ui *UI) initWindow() {
	settings := glLib.NewWindowSettings()
	settings.Name = "File Selector Test"
	settings.Width = 720
	settings.Height = 600
	settings.UseGl = true
	settings.Vsync = true
	window, err := glLib.NewWindow(settings)
	if err != nil {
		log.Fatalln("Failed to create window with error: ", err.Error())
	}
	ui.Window = window
	ui.Window = window
	ui.Ctx = imgui.CreateContext(nil)
	ui.Io = imgui.CurrentIO()
	ui.Platform = imguiImpl.NewSDL(ui.Io, window.Window)
	ui.Renderer = imguiImpl.NewOpenGL3(ui.Io)
}

func (ui *UI) Run() {
	for ui.running {
		ui.input()
		ui.update()
		ui.draw()
		ui.fps.Update()
	}
}

func (ui *UI) Close() {
	ui.Renderer.Dispose()
	ui.Window.Free()
}

func (ui *UI) input() {
	for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
		ui.Platform.ProcessEvent(event)
		switch event.(type) {
		case *sdl.QuitEvent:
			ui.running = false
		}
		ui.selector.Input(event)
	}
}

func (ui *UI) draw() {
	ui.Platform.NewFrame()
	imgui.NewFrame()
	ui.Renderer.PreRender([3]float32{})
	imgui.SetNextWindowSizeV(imgui.Vec2{}, imgui.ConditionAppearing)
	imgui.BeginV("Opener", nil, imgui.WindowFlagsNoTitleBar)
	imgui.Text("FPS: " + strconv.FormatUint(uint64(ui.fps.GetFps()), 10))
	if imgui.Button("Open") {
		if !ui.selector.IsOpen() {
			ui.initSelector()
		}
	}
	imgui.End()
	ui.selector.Draw()
	imgui.Render()
	ui.Renderer.Render(ui.Platform.DisplaySize(), ui.Platform.FramebufferSize(), imgui.RenderedDrawData())
	ui.Window.Window.GLSwap()
}

func (ui *UI) update() {
	ui.selector.Update()
}

func init() {
	runtime.LockOSThread()
}

func main() {
	fd, err := os.OpenFile("data.profile", os.O_CREATE|os.O_RDWR, 0777)
	if err != nil {
		log.Fatalln("Failed to open profiler with error ", err)
	}
	err = pprof.StartCPUProfile(fd)
	if err != nil {
		log.Fatalln("Failed to open profiler with err ", err)
	}
	ui := NewUI()
	ui.Run()
	ui.Close()
	f, err := os.OpenFile("mem.profile", os.O_CREATE|os.O_RDWR, 0777)
	if err != nil {
		log.Fatalln("Failed to create heap profiler with error ", err)
	}
	pprof.WriteHeapProfile(f)
	f.Close()
	pprof.StopCPUProfile()
	fd.Close()
}
